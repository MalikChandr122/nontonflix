package com.nontonflix.aggregator.service;

import com.nontonflix.aggregator.dto.RecommendationResponse;
import com.nontonflix.aggregator.dto.UserResponse;

public interface UserService {
    UserResponse findById(Long id);
    RecommendationResponse findUserMovieRecommendation(Long id);
}
