package com.nontonflix.aggregator.service.impl;

import com.google.common.base.Strings;
import com.nontonflix.aggregator.dto.MovieResponse;
import com.nontonflix.aggregator.service.MovieService;
import com.nontonflix.aggregator.validator.Validator;
import com.nontonflix.movie.MoviePaginationRequest;
import com.nontonflix.movie.MovieSearchRequest;
import com.nontonflix.movie.MovieServiceGrpc;
import com.nontonflix.user.SearchHistoryRequest;
import com.nontonflix.user.SearchHistoryServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {
    @GrpcClient("movie-service")
    private MovieServiceGrpc.MovieServiceBlockingStub movieServiceBlockingStub;

    @GrpcClient("user-service")
    private SearchHistoryServiceGrpc.SearchHistoryServiceBlockingStub searchHistoryServiceBlockingStub;

    @Override
    public List<MovieResponse> getAllLatestMovies(int page) {
        MoviePaginationRequest moviePaginationRequest = MoviePaginationRequest.newBuilder().setPage(page).setPageSize(10).build();
        com.nontonflix.movie.MovieResponse movieResponse = movieServiceBlockingStub.getAllLatestMovies(moviePaginationRequest);
        return movieResponse.getMovieList()
                .stream()
                .map(movieDTO -> new MovieResponse(movieDTO.getId(), movieDTO.getTitle(), movieDTO.getDescription(), movieDTO.getRelease(), movieDTO.getGenre(), movieDTO.getRating()))
                .collect(Collectors.toList());
    }

    @Override
    public List<MovieResponse> getMoviesBySearch(String movieName, String movieGenre) {
        MovieSearchRequest movieSearchRequest = MovieSearchRequest.newBuilder().setName(Objects.toString(movieName, "")).setGenre(Objects.toString(movieGenre, "")).build();
        com.nontonflix.movie.MovieResponse moviesBySearch = movieServiceBlockingStub.getMoviesBySearch(movieSearchRequest);

        List<MovieResponse> movieResponses = moviesBySearch.getMovieList()
                .stream()
                .map(movieDTO -> new MovieResponse(movieDTO.getId(), movieDTO.getTitle(), movieDTO.getDescription(), movieDTO.getRelease(), movieDTO.getGenre(), movieDTO.getRating()))
                .collect(Collectors.toList());

        // Insert into search history for user's movie recommendations, if user already logged in
        if (Validator.isLogged() && !movieResponses.isEmpty() && !Strings.isNullOrEmpty(movieGenre)) {
            searchHistoryServiceBlockingStub.saveSearchHistory(SearchHistoryRequest.newBuilder()
                    .setUserId(Long.valueOf(Validator.getUserData().get("userId").toString()))
                    .setGenre(movieGenre).build());
        }
        return movieResponses;
    }

}
