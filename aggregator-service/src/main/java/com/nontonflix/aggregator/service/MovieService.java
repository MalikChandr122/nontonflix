package com.nontonflix.aggregator.service;

import com.nontonflix.aggregator.dto.MovieResponse;

import java.util.List;

public interface MovieService {
    List<MovieResponse> getAllLatestMovies(int page);
    List<MovieResponse> getMoviesBySearch(String movieName, String movieGenre);
}
