package com.nontonflix.aggregator.service.impl;

import com.nontonflix.aggregator.dto.MovieResponse;
import com.nontonflix.aggregator.dto.RecommendationResponse;
import com.nontonflix.aggregator.dto.UserResponse;
import com.nontonflix.aggregator.service.UserService;
import com.nontonflix.movie.MovieRecommendationRequest;
import com.nontonflix.movie.MovieServiceGrpc;
import com.nontonflix.user.*;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @GrpcClient("user-service")
    private UserServiceGrpc.UserServiceBlockingStub userServiceBlockingStub;

    @GrpcClient("user-service")
    private SearchHistoryServiceGrpc.SearchHistoryServiceBlockingStub searchHistoryServiceBlockingStub;

    @GrpcClient("movie-service")
    private MovieServiceGrpc.MovieServiceBlockingStub movieServiceBlockingStub;

    @Override
    public UserResponse findById(Long id) {
        UserRequest userRequest = UserRequest.newBuilder().setUserId(id).build();
        try {
            com.nontonflix.user.UserResponse user = userServiceBlockingStub.getUserByID(userRequest);
            return new UserResponse(user.getUserDto().getId(), user.getUserDto().getName(), user.getUserDto().getEmail());
        }catch (Exception e) {
            return new UserResponse();
        }
    }

    @Override
    public RecommendationResponse findUserMovieRecommendation(Long id) {
        SearchHistoryRequest searchHistoryRequest = SearchHistoryRequest.newBuilder().setUserId(id).build();
        SearchHistoryResponse searchHistoryResponse = searchHistoryServiceBlockingStub.getSearchHistoryUser(searchHistoryRequest);

        com.nontonflix.movie.MovieResponse moviesRecommendation = movieServiceBlockingStub.getMoviesRecommendation(MovieRecommendationRequest.newBuilder().setGenre(searchHistoryResponse.getGenre()).build());
        List<MovieResponse> movieResponses = moviesRecommendation.getMovieList()
                .stream()
                .map(movieDTO -> new MovieResponse(movieDTO.getId(), movieDTO.getTitle(), movieDTO.getDescription(), movieDTO.getRelease(), movieDTO.getGenre(), movieDTO.getRating()))
                .collect(Collectors.toList());

       return new RecommendationResponse(movieResponses);
    }
}
