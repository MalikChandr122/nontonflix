package com.nontonflix.aggregator.controller;

import com.nontonflix.aggregator.dto.MovieResponse;
import com.nontonflix.aggregator.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/movies/page/{page}")
    public List<MovieResponse> getAllLatestMovies(@PathVariable int page) {
        return movieService.getAllLatestMovies(page);
    }

    @GetMapping("/movies/search")
    public List<MovieResponse> getMoviesBySearch(
            @RequestParam(required = false, name = "s") String movieName,
            @RequestParam(required = false, name = "genre") String movieGenre) {
        return movieService.getMoviesBySearch(movieName, movieGenre);
    }
}
