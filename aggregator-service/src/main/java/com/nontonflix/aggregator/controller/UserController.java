package com.nontonflix.aggregator.controller;

import com.nontonflix.aggregator.dto.MovieResponse;
import com.nontonflix.aggregator.dto.RecommendationResponse;
import com.nontonflix.aggregator.dto.UserResponse;
import com.nontonflix.aggregator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users/{userId}")
    public UserResponse getuserById(@PathVariable Long userId) {
        return userService.findById(userId);
    }

    @GetMapping("/users/movies/recommendation/{userId}")
    public RecommendationResponse getUserMovieRecommendation(@PathVariable Long userId) {
        return userService.findUserMovieRecommendation(userId);
    }

}
