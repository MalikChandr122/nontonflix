package com.nontonflix.aggregator.validator;

import java.util.HashMap;
import java.util.Map;

public class Validator {
    public static boolean isLogged() {
        return true;
    }

    public static Map<String, Object> getUserData() {
        Map<String, Object> userData = new HashMap<>();
        userData.put("userId", 3);
        userData.put("userName", "irithel");
        userData.put("userEmail", "irithel@gmail.com");
        return userData;
    }
}
