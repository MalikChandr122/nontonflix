This project create on JDK 8.

The Database using MySQL, source of table is already available in DB folder.

To run this project, you can run independently using Dockerfile in each services root folder by following this command in terminal/cmd:

docker build -t aggregator-services .
docker run -p 8080:8080 aggregator-services

docker build -t movie-services .
docker run -p 1111:1111 movie-services

docker build -t user-services .
docker run -p 2222:2222 user-services


Or if you want run all services directly, you can run by docker-compose file in root of this folder, using this command:

docker compose up --build

API handled by aggregation service, the communication between aggregation service with movie and user service using gRPC. So the client (Frontend) only call API from aggregation service.

