package com.nontonflix.movie.repository;

import com.nontonflix.movie.entity.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    @Query(
            value = "select * from tb_movies where movie_title = ?1 or movie_genre = ?2",
            nativeQuery = true
    )
    List<Movie> getMoviesBySearch(String title, String genre);
    Page<Movie> findAll(Pageable pageable);
    List<Movie> findAllByMovieGenre(String genre);
}
