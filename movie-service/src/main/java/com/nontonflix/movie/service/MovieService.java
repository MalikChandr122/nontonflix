package com.nontonflix.movie.service;

import com.google.common.base.Strings;
import com.nontonflix.movie.*;
import com.nontonflix.movie.entity.Movie;
import com.nontonflix.movie.repository.MovieRepository;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@GrpcService
public class MovieService extends MovieServiceGrpc.MovieServiceImplBase {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public void getMoviesRecommendation(MovieRecommendationRequest request, StreamObserver<MovieResponse> responseObserver) {
        List<Movie> allByMovieGenre = movieRepository.findAllByMovieGenre(request.getGenre());
        List<MovieDTO> movieDTOList = allByMovieGenre
                .stream()
                .map(movie -> MovieDTO.newBuilder()
                        .setId(movie.getId())
                        .setTitle(movie.getMovieTitle())
                        .setDescription(movie.getMovieDescription())
                        .setGenre(movie.getMovieGenre())
                        .setRelease(new SimpleDateFormat("dd-MM-yyyy").format(movie.getMovieRelease()))
                        .setRating(movie.getMovieRating()).build())
                .collect(Collectors.toList());
        responseObserver.onNext(MovieResponse.newBuilder().addAllMovie(movieDTOList).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getMoviesBySearch(MovieSearchRequest request, StreamObserver<MovieResponse> responseObserver) {
        List<Movie> moviesBySearch = movieRepository.getMoviesBySearch(Strings.emptyToNull(request.getName()), Strings.emptyToNull(request.getGenre()));
        List<MovieDTO> movieDTOList = moviesBySearch
                .stream()
                .map(movie -> MovieDTO.newBuilder()
                        .setId(movie.getId())
                        .setTitle(movie.getMovieTitle())
                        .setDescription(movie.getMovieDescription())
                        .setGenre(movie.getMovieGenre())
                        .setRelease(new SimpleDateFormat("dd-MM-yyyy").format(movie.getMovieRelease()))
                        .setRating(movie.getMovieRating()).build())
                .collect(Collectors.toList());
        responseObserver.onNext(MovieResponse.newBuilder().addAllMovie(movieDTOList).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getAllLatestMovies(MoviePaginationRequest request, StreamObserver<MovieResponse> responseObserver) {
        Page<Movie> allLatestMovie = movieRepository.findAll(PageRequest.of(request.getPage(), request.getPageSize(), Sort.by(Sort.Direction.DESC, "movieRelease")));
        List<MovieDTO> collect = allLatestMovie.getContent()
                .stream()
                .map(movie -> MovieDTO.newBuilder()
                        .setId(movie.getId())
                        .setTitle(movie.getMovieTitle())
                        .setDescription(movie.getMovieDescription())
                        .setGenre(movie.getMovieGenre())
                        .setRelease(new SimpleDateFormat("dd-MM-yyyy").format(movie.getMovieRelease()))
                        .setRating(movie.getMovieRating()).build())
                .collect(Collectors.toList());
        responseObserver.onNext(MovieResponse.newBuilder().addAllMovie(collect).build());
        responseObserver.onCompleted();
    }
}
