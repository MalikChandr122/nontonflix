package com.nontonflix.movie.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "tb_movies")
@Data
@ToString
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "movie_id")
    private String movieId;

    @Column(name = "movie_title")
    private String movieTitle;

    @Column(name = "movie_description")
    private String movieDescription;

    @Column(name = "movie_url")
    private String movieUrl;

    @Column(name = "movie_rating")
    private Double movieRating;

    @Column(name = "movie_genre")
    private String movieGenre;

    @Column(name = "movie_release")
    private Date movieRelease;
}
