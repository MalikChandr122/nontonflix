-- -------------------------------------------------------------
-- TablePlus 5.0.2(458)
--
-- https://tableplus.com/
--
-- Database: db_movies
-- Generation Time: 2022-10-26 13:49:50.5030
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE TABLE `tb_movies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `movie_id` varchar(255) DEFAULT NULL,
  `movie_title` varchar(255) DEFAULT NULL,
  `movie_description` varchar(255) DEFAULT NULL,
  `movie_url` varchar(255) DEFAULT NULL,
  `movie_rating` double DEFAULT NULL,
  `movie_genre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `movie_release` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `tb_movies` (`id`, `movie_id`, `movie_title`, `movie_description`, `movie_url`, `movie_rating`, `movie_genre`, `movie_release`) VALUES
(1, 'vid121', 'James Bonds 1', 'This is description of movie', 'https://www.linkmovie.com', 9.2, 'Action', '2011-12-12 00:00:00'),
(2, 'vid122', 'Jackie Chan', 'This is description of movie', 'https://www.linkmovie.com', 5, 'Action', '2020-12-12 00:00:00'),
(3, 'vid123', 'Barbarian', 'This is description of movie', 'https://www.linkmovie.com', 3.3, 'Thriller', '2021-12-12 00:00:00'),
(4, 'vid124', 'Mr Bean', 'This is description of movie', 'https://www.linkmovie.com', 8.5, 'Comedy', '2015-12-12 00:00:00'),
(5, 'vid125', 'Korean Love', 'This is description of movie', 'https://www.linkmovie.com', 2.4, 'Drama', '2012-12-12 00:00:00'),
(6, 'vid126', 'Final Fantasy', 'This is description of movie', 'https://www.linkmovie.com', 6.7, 'Fantasy', '2013-12-12 00:00:00'),
(7, 'vid127', 'Horror Movie', 'This is description of movie', 'https://www.linkmovie.com', 7.3, 'Crime', '2017-12-12 00:00:00'),
(8, 'vid128', 'The Legend Of Messi', 'This is description of movie', 'https://www.linkmovie.com', 9.5, 'Sport', '2016-12-12 00:00:00'),
(9, 'vid129', 'Story Of Me', 'This is description of movie', 'https://www.linkmovie.com', 8.5, 'Sport', '2012-12-12 00:00:00'),
(10, 'vid1210', 'Black Adam', 'This is description of movie', 'https://www.linkmovie.com', 9.2, 'Action', '2019-12-12 00:00:00'),
(11, 'vid1211', 'Interstellar', 'This is description of movie', 'https://www.linkmovie.com', 8.2, 'Sci-fi', '2000-12-12 00:00:00'),
(12, 'vid1212', 'Detective Conan', 'This is description of movie', 'https://www.linkmovie.com', 9.2, 'Action', '2002-12-12 00:00:00'),
(13, 'vid1213', 'Love You', 'This is description of movie', 'https://www.linkmovie.com', 9.1, 'Romance', '2003-12-12 00:00:00'),
(14, 'vid1214', 'Misi', 'This is description of movie', 'https://www.linkmovie.com', 8.4, 'Action', '2010-12-12 00:00:00'),
(15, 'vid1215', 'Angel Attack', 'This is description of movie', 'https://www.linkmovie.com', 4.1, 'Action', '2005-12-12 00:00:00'),
(16, 'vid1216', 'What This House', 'This is description of movie', 'https://www.linkmovie.com', 5.8, 'Mystery', '2006-12-12 00:00:00'),
(17, 'vid1217', 'Jumanji', 'This is description of movie', 'https://www.linkmovie.com', 6.6, 'Mystery', '2007-12-12 00:00:00'),
(18, 'vid1218', 'Bullet Train', 'This is description of movie', 'https://www.linkmovie.com', 7.5, 'Action', '2010-12-12 00:00:00'),
(19, 'vid1219', 'Top Gun', 'This is description of movie', 'https://www.linkmovie.com', 6.6, 'Action', '2008-12-12 00:00:00'),
(20, 'vid1220', 'Love Me', 'This is description of movie', 'https://www.linkmovie.com', 4.3, 'Romance', '2009-12-12 00:00:00');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;