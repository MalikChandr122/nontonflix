package com.nontonflix.user.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "tb_search_history")
@Data
@ToString
public class SearchHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "genre")
    private String genre;

    @Column(name = "search_date")
    private Date searchDate;
}
