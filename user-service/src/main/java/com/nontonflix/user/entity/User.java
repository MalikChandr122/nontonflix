package com.nontonflix.user.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity(name = "tb_users")
@Data
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_email")
    private String userEmail;
}
