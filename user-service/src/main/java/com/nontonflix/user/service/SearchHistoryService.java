package com.nontonflix.user.service;

import com.google.protobuf.Empty;
import com.nontonflix.user.SearchHistoryRequest;
import com.nontonflix.user.SearchHistoryResponse;
import com.nontonflix.user.SearchHistoryServiceGrpc;
import com.nontonflix.user.entity.SearchHistory;
import com.nontonflix.user.repository.SearchHistoryRepository;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@GrpcService
public class SearchHistoryService extends SearchHistoryServiceGrpc.SearchHistoryServiceImplBase {
    @Autowired
    private SearchHistoryRepository searchHistoryRepository;

    @Override
    public void getSearchHistoryUser(SearchHistoryRequest request, StreamObserver<SearchHistoryResponse> responseObserver) {
        SearchHistory searchHistory = searchHistoryRepository.findFirstByUserIdOrderBySearchDateDesc(request.getUserId());
        if (searchHistory==null)
            responseObserver.onNext(SearchHistoryResponse.newBuilder().build());
        else
            responseObserver.onNext(SearchHistoryResponse.newBuilder().setGenre(searchHistory.getGenre()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void saveSearchHistory(SearchHistoryRequest request, StreamObserver<Empty> responseObserver) {
        SearchHistory searchHistory = new SearchHistory();
        searchHistory.setUserId(request.getUserId());
        searchHistory.setGenre(request.getGenre());
        searchHistory.setSearchDate(new Date());
        searchHistoryRepository.save(searchHistory);
        responseObserver.onNext(null);
        responseObserver.onCompleted();
    }
}
