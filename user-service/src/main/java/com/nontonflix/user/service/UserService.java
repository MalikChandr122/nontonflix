package com.nontonflix.user.service;

import com.nontonflix.user.UserDTO;
import com.nontonflix.user.UserRequest;
import com.nontonflix.user.UserResponse;
import com.nontonflix.user.UserServiceGrpc;
import com.nontonflix.user.entity.User;
import com.nontonflix.user.repository.UserRepository;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@GrpcService
public class UserService extends UserServiceGrpc.UserServiceImplBase {
    @Autowired
    UserRepository userRepository;

    @Override
    public void getUserByID(UserRequest request, StreamObserver<UserResponse> responseObserver) {
        Optional<User> optionalUser = userRepository.findById(request.getUserId());
        if (optionalUser.isPresent()) {
            responseObserver.onNext(UserResponse.newBuilder().setUserDto(UserDTO.newBuilder()
                    .setId(optionalUser.get().getId())
                    .setEmail(optionalUser.get().getUserEmail())
                    .setName(optionalUser.get().getUserName())).build());
        }else {
            responseObserver.onError(new Exception("User not found"));
        }
        responseObserver.onCompleted();
    }
}
