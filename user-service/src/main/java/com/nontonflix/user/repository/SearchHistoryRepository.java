package com.nontonflix.user.repository;

import com.nontonflix.user.entity.SearchHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SearchHistoryRepository extends JpaRepository<SearchHistory, Long> {
    SearchHistory findFirstByUserIdOrderBySearchDateDesc(Long userId);
}
